package com.example.nikita.weatherapp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Nikita on 18.05.2018.
 */

public class DateHelper {
    static String getDayOfWeek(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("E", Locale.ENGLISH);
        return String.valueOf(dateFormat.format(date));
    }

    static int getHours (Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH", Locale.getDefault());
        return Integer.parseInt(dateFormat.format(date));
    }

    static String getMonthWithDay(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
        return dateFormat.format(date);
    }
}
