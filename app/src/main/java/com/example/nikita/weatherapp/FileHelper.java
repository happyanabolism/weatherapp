package com.example.nikita.weatherapp;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * Created by Nikita on 15.05.2018.
 */

public class FileHelper {

    public void saveJsonData(Context context, JSONObject jsonObject, String fileName) {
        //write json data to file

        String data = jsonObject.toString();

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(
                    context.openFileOutput(fileName, Context.MODE_PRIVATE)));
            bw.write(data);
            Log.d("weathertag", "data successfully writed to file");
        } catch (IOException e) { }
        finally {
            try {
                bw.close();
            } catch (IOException e) { }
        }

    }

    public JSONObject loadJsonData(Context context, String fileName) {
        //read json data from file

        BufferedReader br = null;
        JSONObject jsonData = null;
        try {
            br = new BufferedReader(new InputStreamReader(
                    context.openFileInput(fileName)));

            StringBuilder data = new StringBuilder();
            String tmp = "";
            while((tmp = br.readLine()) != null) {
                data.append(tmp).append("\n");
            }
            jsonData = new JSONObject(data.toString());
            Log.d("weathertag", "data was successfully readed from file");
        } catch (IOException e) { }
        finally {
            try {
                br.close();
            } catch (IOException e) { }
            finally {
                return jsonData;
            }
        }

    }
}
