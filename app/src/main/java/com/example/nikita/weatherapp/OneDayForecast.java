package com.example.nikita.weatherapp;

import java.util.Date;

/**
 * Created by Nikita on 18.05.2018.
 */

public class OneDayForecast {
    private Date date;
    private String weatherDescription;
    private String codeImage;
    private int humidity;
    private int windSpeed;
    private int temperature;
    private int pressure;
    private String cityName;
    private String countryName;

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public String getCodeImage() {
        return codeImage;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public OneDayForecast(Date date, String weatherDescription, String codeImage, int humidity,
                          int windSpeed, int temperature, int pressure, String cityName, String countryName) {
        this.date = date;
        this.weatherDescription = weatherDescription;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.temperature = temperature;
        this.pressure = pressure;
        this.codeImage = codeImage;
        this.cityName = cityName;
        this.countryName = countryName;


    }

    public Date getDate() {
        return date;
    }
}
