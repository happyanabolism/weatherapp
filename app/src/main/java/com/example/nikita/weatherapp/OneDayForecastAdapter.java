package com.example.nikita.weatherapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Nikita on 18.05.2018.
 */

public class OneDayForecastAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<OneDayForecast> forecast;
    private LayoutInflater layout;

    public OneDayForecastAdapter(Context context, ArrayList<OneDayForecast> forecast) {
        this.context = context;
        this.forecast = forecast;
        this.layout = (LayoutInflater)this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.forecast.size();
    }

    @Override
    public Object getItem(int position) {
        return this.forecast.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = this.layout.inflate(R.layout.one_day_weather_item, parent, false);
        }

        OneDayForecast forecast = getForecastItem(position);

        ((TextView)view.findViewById(R.id.dayOfWeekText))
                .setText(DateHelper.getDayOfWeek(forecast.getDate()).toLowerCase());
        ((TextView)view.findViewById(R.id.dateText))
                .setText(DateHelper.getMonthWithDay(forecast.getDate()));
        ((TextView)view.findViewById(R.id.humidity_text))
                .setText(String.format(Locale.getDefault(), "%d%%", forecast.getHumidity()));
        ((TextView)view.findViewById(R.id.wind_speed_text))
                .setText(String.format(Locale.getDefault(), "%dm/s", forecast.getWindSpeed()));
        ((TextView)view.findViewById(R.id.pressure_text))
                .setText(String.format(Locale.getDefault(), "%dmm", forecast.getPressure()));
        ((TextView)view.findViewById(R.id.weather_text_small)).setText(forecast.getWeatherDescription());
        ((TextView)view.findViewById(R.id.temperature_text))
                .setText(String.format(Locale.getDefault(), "%d°C", forecast.getTemperature()));
        ((ImageView)view.findViewById(R.id.weather_img_sm)).setImageResource(chooseIcon(forecast));


        return view;
    }

    private int chooseIcon(OneDayForecast forecast) {
        String imgCode = forecast.getCodeImage();
        int image;

        switch (imgCode) {
            case "01d":
                image = R.drawable.sunny_small;
                break;
            case "01n":
                image = R.drawable.moon_small;
                break;
            case "02d":
                image = R.drawable.cloudy_sun_small;
                break;
            case "02n":
                image = R.drawable.cloudy_night_small;
                break;
            case "03d":
            case "03n":
                image = R.drawable.cloudy_small;
                break;
            case "04d":
            case "04n":
                image = R.drawable.few_clouds_small;
                break;
            case "09d":
            case "09n":
                image = R.drawable.rainy_small;
                break;
            case "10d":
                image = R.drawable.rainy_sun_small;
                break;
            case "10n":
                image = R.drawable.rainy_night_small;
                break;
            case "11d":
            case "11n":
                image = R.drawable.stormy_small;
                break;
            case "13d":
            case "13n":
                image = R.drawable.snow_small;
                break;
            default:
                image = R.drawable.sunny_small;
                break;
        }

        return image;
    }

    OneDayForecast getForecastItem(int position) {
        return ((OneDayForecast) getItem(position));
    }
}
