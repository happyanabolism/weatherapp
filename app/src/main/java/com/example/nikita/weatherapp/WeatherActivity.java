package com.example.nikita.weatherapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.content.DialogInterface;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;

import java.util.ArrayList;
import java.util.Locale;

public class WeatherActivity extends AppCompatActivity {

    TextView countryTextV;
    TextView cityTextV;
    TextView weatherBigTextV;
    ImageView weatherBigImageV;
    ListView forecastListV;
    Button updateButton;

    Context context;

    ArrayList<OneDayForecast> weatherForecast;
    WeatherForecast forecast;
    OneDayForecast actualWeather;

    private LocationManager locationManager;
    private Pair<Double, Double> coordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        context = this;

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);


        initViews();

        coordinates = getLocation();

        forecast = new WeatherForecast(this, coordinates);
        weatherForecast = forecast.getForecast();
        actualWeather = forecast.getActualForecast();
        if (weatherForecast == null) {
            buildAlertMessageNoGpsAndNetwork();
        } else {
            updateBigView();
        }
    }

    private void initViews() {
        countryTextV = (TextView) findViewById(R.id.country_text);
        cityTextV = (TextView) findViewById(R.id.city_text);
        weatherBigTextV = (TextView) findViewById(R.id.weather_text_big);
        weatherBigImageV = (ImageView) findViewById(R.id.weather_img_big);
        forecastListV = (ListView) findViewById(R.id.forecast_list_view);
        updateButton = (Button)findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coordinates = getLocation();

                forecast = new WeatherForecast(context, coordinates);
                weatherForecast = forecast.getForecast();
                actualWeather = forecast.getActualForecast();
                forecastListV.setAdapter(new OneDayForecastAdapter(context, weatherForecast));
                updateBigView();
            }
        });
    }

    private void updateBigView() {
        weatherBigImageV.setImageResource(chooseWeatherImage(actualWeather));
        weatherBigTextV.setText(String.format(Locale.getDefault(), "%s %d°C",
                actualWeather.getWeatherDescription(), actualWeather.getTemperature()));

        cityTextV.setText(actualWeather.getCityName());
        countryTextV.setText(actualWeather.getCountryName());


        forecastListV.setAdapter(new OneDayForecastAdapter(this, weatherForecast));
    }

    private Pair<Double, Double> getLocation() {
        Pair<Double, Double> coordinates = null;
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (checkAccessLocationPermission()) {
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    coordinates = new Pair<>(latitude, longitude);
                }
            }
        }
        return coordinates;
    }

    private boolean checkAccessLocationPermission() {
        return !(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }

    private void buildAlertMessageNoGpsAndNetwork() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your Network and GPS connection and update")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, int id) {
                    }
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private int chooseWeatherImage(OneDayForecast forecast) {
        String imgCode = forecast.getCodeImage();
        int image;

        switch (imgCode) {
            case "01d":
                image = R.drawable.sunny_big;
                break;
            case "01n":
                image = R.drawable.moon_big;
                break;
            case "02d":
                image = R.drawable.cloudy_sun_big;
                break;
            case "02n":
                image = R.drawable.cloudy_night_big;
                break;
            case "03d":
            case "03n":
                image = R.drawable.cloudy_big;
                break;
            case "04d":
            case "04n":
                image = R.drawable.few_clouds_big;
                break;
            case "09d":
            case "09n":
                image = R.drawable.rainy_big;
                break;
            case "10d":
                image = R.drawable.rainy_sun_small;
                break;
            case "10n":
                image = R.drawable.rainy_night_big;
                break;
            case "11d":
            case "11n":
                image = R.drawable.stormy_big;
                break;
            case "13d":
            case "13n":
                image = R.drawable.snow_big;
                break;
            default:
                image = R.drawable.sunny_big;
                break;
        }

        return image;
    }

}
