package com.example.nikita.weatherapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Nikita on 17.05.2018.
 */

public class WeatherDataJsonParser {
    private JSONObject weatherData;

    public WeatherDataJsonParser(JSONObject weatherData) {
        this.weatherData = weatherData;
    }

    private String getWeatherDescription(JSONObject object){
        String weatherDescription = null;
        try {
            weatherDescription = object
                    .getJSONArray("weather")
                    .getJSONObject(0)
                    .getString("main");
        } catch (Exception e) { }

        return weatherDescription;
    }

    private String getImageCode(JSONObject object) {
        String code = null;
        try {
            code = object
                    .getJSONArray("weather")
                    .getJSONObject(0)
                    .getString("icon");
        } catch (Exception e) { }

        return code;
    }

    private int getTemperature(JSONObject object) {
        int temperature = 0;
        try {
            temperature = object
                    .getJSONObject("main")
                    .getInt("temp");
        } catch (Exception e) { }

        return temperature;
    }

    private int getPressure(JSONObject object) {
        int pressure = 0;
        try {
            pressure = object
                    .getJSONObject("main")
                    .getInt("pressure");
        } catch (Exception e) { }

        return pressure;
    }

    private int getHumidity(JSONObject object) {
        int humidity = 0;
        try {
            humidity = object
                    .getJSONObject("main")
                    .getInt("humidity");
        } catch (Exception e) { }

        return humidity;
    }

    private int getWindSpeed(JSONObject object) {
        int windSpeed = 0;
        try {
            windSpeed = object
                    .getJSONObject("wind")
                    .getInt("speed");
        } catch (Exception e) { }

        return windSpeed;
    }

    public Date getDate(JSONObject object) {
        String dateStr = null;
        String format = "yyyy-MM-dd HH:mm:ss";
        Date date = null;
        try {
            dateStr = object.getString("dt_txt");
        } catch (Exception e) { }

        SimpleDateFormat df = new SimpleDateFormat(format);
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) { }

        return date;
    }

    private String getCountryName(JSONObject object) {
        String countryName = null;
        try {
            countryName = object.getString("country");
        } catch (Exception e) {}

        return countryName;
    }

    private String getCityName(JSONObject object) {
        String cityName = null;
        try {
            cityName = object.getString("city");
        } catch (Exception e) {}

        return cityName;
    }

    public ArrayList<OneDayForecast> getForecast() throws JSONException {
        if (weatherData == null) {
            return null;
        }

        int midday = 12;
        ArrayList<OneDayForecast> forecast = new ArrayList<>();

        JSONArray list = weatherData.getJSONArray("list");

        for (int i = 0; i < list.length(); i++) {
            if (DateHelper.getHours(getDate(list.getJSONObject(i))) == midday) {
                JSONObject object = list.getJSONObject(i);

                forecast.add(new OneDayForecast(getDate(object), getWeatherDescription(object), getImageCode(object),
                        getHumidity(object), getWindSpeed(object), getTemperature(object), getPressure(object),
                        getCityName(weatherData), getCountryName(weatherData)));
            }
        }

        return forecast;
    }

    public OneDayForecast getActualForecast() throws JSONException {
        if (weatherData == null) {
            return null;
        }

        JSONObject object = weatherData.getJSONArray("list").getJSONObject(0);
        return new OneDayForecast(getDate(object), getWeatherDescription(object), getImageCode(object),
                getHumidity(object), getWindSpeed(object), getTemperature(object), getPressure(object),
                getCityName(weatherData), getCountryName(weatherData));
    }
}
