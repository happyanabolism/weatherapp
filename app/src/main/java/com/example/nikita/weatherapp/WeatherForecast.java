package com.example.nikita.weatherapp;

import android.app.Application;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Nikita on 18.05.2018.
 */

public class WeatherForecast {
    private final String STORED_WEATHER_FILENAME = "stored_weather.json";

    Context context;

    private WeatherDataJsonParser weatherDataJsonParser;
    private ArrayList<OneDayForecast> forecast;

    public WeatherForecast(Context context, Pair<Double, Double> coordinates) {
        this.context = context;

        FileHelper fh = new FileHelper();

        String cityName = null;
        String countryName = null;
        if (coordinates != null) {
            Address address = getAddressByCoordinates(context, coordinates);
            cityName = address.getLocality();
            countryName = address.getCountryName();
        }


        JSONObject weatherJSON = null;
        try {
            weatherJSON = new WeatherRequestTask().execute(coordinates.first,
                    coordinates.second).get();
        } catch (Exception e) {  }

        if (weatherJSON != null && coordinates != null) {
            try {
                weatherJSON.put("city", cityName);
                weatherJSON.put("country", countryName);
            } catch (JSONException e) { }
            fh.saveJsonData(context, weatherJSON, STORED_WEATHER_FILENAME);
        } else {
            weatherJSON = fh.loadJsonData(context, STORED_WEATHER_FILENAME);
        }

        weatherDataJsonParser = new WeatherDataJsonParser(weatherJSON);
    }

    private Address getAddressByCoordinates(Context context, Pair<Double, Double> coordinates) {
        Geocoder geocoder = new Geocoder(context, Locale.UK);
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(
                    coordinates.first, coordinates.second, 1);
        } catch (Exception e)  { Log.d("weathertag", "geocoder exception"); return null;}
        return addresses.get(0);
    }

    public ArrayList<OneDayForecast> getForecast() {
        try {
            return weatherDataJsonParser.getForecast();
        } catch (Exception e) {}
        return null;
    }

    public OneDayForecast getActualForecast() {
        try {
            return weatherDataJsonParser.getActualForecast();
        } catch (Exception e) {}
        return null;
    }

}
