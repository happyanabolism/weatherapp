package com.example.nikita.weatherapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by Nikita on 12.05.2018.
 */

public class WeatherRequestTask extends AsyncTask<Double, Void, JSONObject> {

    private final String OPEN_WEATHER_MAP_API =
            "http://api.openweathermap.org/data/2.5/forecast?lat=%f&lon=%f&units=metric";

    private final String OPEN_WEATHER_MAP_API_KEY =
            "b5711ae3b88909f8261cd1ac738ca600";


    public JSONObject getWeatherData(double latitude, double longitude) {
        try {
            URL url = new URL(String.format(Locale.getDefault(), OPEN_WEATHER_MAP_API,
                    latitude, longitude));

            HttpURLConnection connection =
                    (HttpURLConnection)url.openConnection();

            connection.addRequestProperty("x-api-key", OPEN_WEATHER_MAP_API_KEY);

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String tmp = "";
            while((tmp = reader.readLine()) != null) {
                response.append(tmp).append("\n");
            }
            reader.close();

            JSONObject jsonResponse = new JSONObject(response.toString());

            //check code from response
            if(jsonResponse.getInt("cod") != 200) {
                throw new Exception();
            }

            return jsonResponse;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    protected JSONObject doInBackground(Double... params) {
        return getWeatherData(params[0], params[1]);
    }
}
